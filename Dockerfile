FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN apt-get -y update && apt-get install -y libzbar-dev
RUN pip install --upgrade pip  
RUN pip install -r requirements.txt
COPY . /code/